package People;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.LocalDate;
import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PeopleTests {
    @Test
    void getAge_when_person_bornToday_returns_0(){
        // Arrange
        Person person = new Person("a", "a", LocalDate.now(), 0, 1, Gender.MALE);
        // Act
        int age = person.getAge();
        // Assert
        assertThat(age).isEqualTo(0);
    }
    @ParameterizedTest(name = "\"{0}\"- throws IllegalArgumentException")
    @ValueSource(strings = {"", " "})
    @NullSource
    void new_given_nullFirstName_throws_IllegalArgumentException(String firstName){
        // Act
        Throwable thrown =  catchThrowable(() -> buildPersonWithFirstName(firstName));
        // Assert
        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("firstName");
    }
    private static Person buildPersonWithFirstName(String firstName){
        return new Person(firstName, "a", LocalDate.now(), 1, 1, Gender.MALE);
    }
    //antras komentaras ikeltas
}
