package People;

import People.Gender;

import java.time.LocalDate;
import java.time.Period;

public class Person {
    /*public static final int ADULT_AGE = 18;

    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private float weight;
    private float height;
    private Gender gender;

    public Person(String firstName, String lastName, LocalDate birthday, float weight, float height, Gender gender) {
        *//*if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("firstName cannot be null or empty");
        }*//*
        //kadangi sukurta requires klase
        Requires.Str.NotNullOrEmpty(firstName,"firstname");
        Requires.DateTime.NotFuture(birthday,"birthday");


        this.firstName = firstName;
        setLastName(lastName);
        this.birthday = birthday;
        this.weight = weight;
        this.height = height;
        this.gender = gender;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        *//*if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("firstName cannot be null or empty");
        }*//*
        //kadangi sukurem People.Requires klase
        Requires.Str.NotNullOrEmpty(lastName,"lastname");
        this.lastName = lastName;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge(){
        return Period.between(birthday, LocalDate.now()).getYears();
    }

    @Override
    public String toString() {
        return "People.Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", weight=" + weight +
                ", height=" + height +
                ", gender=" + gender +
                '}';
    }*/
    public static final int ADULT_AGE = 18;
    private String firstName;
    private String lastName;
    private LocalDate birthday;
    private float weight;
    private float height;
    private Gender gender;
    public Person(String firstName, String lastName, LocalDate birthday, float weight, float height, Gender gender) {
        Requires.Str.NotNullOrEmpty(firstName, "firstName");
        Requires.DateTime.NotFuture(birthday, "birthday");
        this.firstName = firstName;
        setLastName(lastName);
        this.birthday = birthday;
        this.weight = weight;
        this.height = height;
        this.gender = gender;
    }
    public String getFullName(){
        return firstName + " " + lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        Requires.Str.NotNullOrEmpty(lastName, "lastName");
        this.lastName = lastName;
    }
    public LocalDate getBirthday() {
        return birthday;
    }
    public float getWeight() {
        return weight;
    }
    public void setWeight(float weight) {
        this.weight = weight;
    }
    public float getHeight() {
        return height;
    }
    public void setHeight(float height) {
        this.height = height;
    }
    public Gender getGender() {
        return gender;
    }
    public int getAge(){
        return Period.between(birthday, LocalDate.now()).getYears();
    }
    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", weight=" + weight +
                ", height=" + height +
                ", gender=" + gender +
                '}';
    }
}



