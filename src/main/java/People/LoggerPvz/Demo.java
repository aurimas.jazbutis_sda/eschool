package People.LoggerPvz;

import People.Gender;
import People.Person;
import org.apache.log4j.*;
import org.apache.log4j.spi.LoggerFactory;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Demo {
    private static Logger logger = LogManager.getLogger("main logger");
    public static void main(String[] args) throws IOException {
        final String format = "%d{yyyy-MM-dd HH:mm:ss,SSS} [%t] %p - %m%n";
        PatternLayout pattern = new PatternLayout(format);
        addConsoleAppender(logger, pattern);
        addFileAppender(logger, pattern);
        //demoPeople();
        demoPeople2();
    }
    private static void addFileAppender(Logger logger, PatternLayout pattern) throws IOException {
        final String logFile = "C:\\Users\\aurim\\Documents\\Aurimas\\JAVA\\java3kaunas\\log.txt";
        //C:\Users\aurim\Documents\Aurimas\JAVA\java3kaunas
        FileAppender fileAppender = new FileAppender(pattern, logFile);
        logger.addAppender(fileAppender);
    }
    private static void addConsoleAppender(Logger logger, Layout layout) {
        ConsoleAppender consoleAppender = new ConsoleAppender(layout);
        logger.addAppender(consoleAppender);
    }

    //Su streamu padaryta
    private static void demoPeople() {
        logger.info("Started people demo");
        Person Aurimas = new Person("Aurimas", "Jazbutis", LocalDate.of(1988, 9, 25), 88, 55, Gender.MALE);
        Person Tomas = new Person("Tomas", "Dzeris", LocalDate.of(2000, 6, 6), 15, 25, Gender.MALE);
        Person James = new Person("James", "Bond", LocalDate.of(2010, 1, 1), 50, 69, Gender.MALE);
        Person Tommy = new Person("Tommy", "Pee",LocalDate.of(1999, 2,3), 87, 65, Gender.MALE);
        Person[] people = {Aurimas, Tomas, James, Tommy};
        Stream<Person> peopleStream = Arrays.stream(people);
        peopleStream
                .filter(p -> p.getAge() >= Person.ADULT_AGE)
                .forEach(p ->
                {
                    System.out.println(p);
                    logger.info("Person over 18: ");
                    logger.info(p);
                });
        logger.info("Ending people demo.");
    }

    //Su listu padaryta
    private static void demoPeople2() {
        logger.info("Started people demo");
        Person Aurimas = new Person("Aurimas", "Jazbutis", LocalDate.of(1988, 9, 25), 88, 55, Gender.MALE);
        Person Tomas = new Person("Tomas", "Dzeris", LocalDate.of(2000, 6, 6), 15, 25, Gender.MALE);
        Person James = new Person("James", "Bond", LocalDate.of(2010, 1, 1), 50, 69, Gender.MALE);
        Person Benas = new Person("Benas", "Benaitis",LocalDate.of(1991, 8,30), 78, 101, Gender.MALE);
        List<Person> zmones = new ArrayList<>();
        zmones.add(Aurimas);
        zmones.add(Tomas);
        zmones.add(James);
        zmones.add(Benas);
        //System.out.println(Aurimas);
        //System.out.println(Tomas);
        //System.out.println(James);
        //logger.info(zmones);
        logger.info(Aurimas);
        logger.info(Tomas);
        logger.info(James);
        logger.info(Benas);
        logger.info("Ending people demo.");
    }
}


