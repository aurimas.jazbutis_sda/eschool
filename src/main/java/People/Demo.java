package People;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Demo {
    public static void main(String[] args) {
        Person Aurimas = new Person("Aurimas", "Jazbutis", LocalDate.of(1988, 9, 25), 88, 55, Gender.MALE);
        Person Tomas = new Person("Tomas", "Dzeris", LocalDate.of(2000, 6, 6), 15, 25, Gender.MALE);
        Person James = new Person("James", "Bond", LocalDate.of(2010, 1, 1), 50, 69, Gender.MALE);
        Person Tommy = new Person("Tommy", "Pee",LocalDate.of(1999, 2,3), 87, 65, Gender.MALE);
//komentaras22
        //Jeigu kuriant lista
        /*List<People.Person> zmones = new ArrayList<>();
        zmones.add(Aurimas);
        zmones.add(Tomas);
        zmones.add(James);
        System.out.println(zmones);*/

        Person[] people = {Aurimas, Tomas, James, Tommy};
        Stream<Person> peopleStream = Arrays.stream(people);
        peopleStream
                .filter(p -> p.getAge() >= Person.ADULT_AGE)
                .forEach(p -> System.out.println(p));
        //People.Person[] adultsArray = adultsStream.toArray(People.Person[] :: new);


        //System.out.println(people);
    }
}
